<div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur blur-rounded top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid">
            <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " href="{{ env('APP_URL') }}">
              oCaisse
            </a>
            <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon mt-2">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </span>
            </button>
            <div class="collapse navbar-collapse w-100 pt-3 pb-2 py-lg-0 text-right" id="navigation">
              <ul class="navbar-nav navbar-nav-hover w-100">
                  <li class="nav-item ms-lg-auto">
                    <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="../">
                      Home
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link me-2" href="../pages/profile.html">
                      Our Features
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link me-2" href="../pages/profile.html">
                      Pricing
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link me-2" href="../pages/sign-up.html">
                      Contact Us
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link me-2" href="../pages/sign-in.html">
                      FAQ
                    </a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link nav-link-icon me-2" href="../signin">
                      <p class="d-inline text-sm z-index-1 font-bold" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Star us on Github">Login</p>
                      </a>
                  </li>
                  <li class="nav-item my-auto ms-3 ms-lg-0">
                      <a href="../signup" class="btn btn-sm  bg-gradient-dark  btn-round mb-0 me-1 mt-2 mt-md-0">Get free trial now!</a>
                  </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>