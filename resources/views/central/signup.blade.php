@extends('central.components.layout')

@section('title', '- Sign Up')

@section('content')
@include('central.components.navbar')
<main class="main-content mt-0">
    <section>
      <div class="page-header min-vh-75">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
              <div class="card card-plain mt-7">
                <div class="card-header pb-0 text-left bg-transparent">
                  <h3 class="font-weight-bolder text-info text-gradient">Welcome!</h3>
                  <p class="mb-0">Start your free trial now!</p>
                </div>
                <div class="card-body">
                  <form role="form">
                    <label>First Name</label>
                    <div class="mb-3">
                      <input type="text" class="form-control" placeholder="First Name" aria-label="First Name" aria-describedby="first-name">
                    </div>
                    <label>Last Name</label>
                    <div class="mb-3">
                      <input type="text" class="form-control" placeholder="Last Name" aria-label="Last Name" aria-describedby="last-name">
                    </div>
                    <label>Company Name</label>                    
                    <div class="mb-3">
                      <input type="text" class="form-control" placeholder="Company Name" aria-label="Company Name" aria-describedby="company-name">
                    </div>
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" placeholder="companyname" aria-label="companyname" aria-describedby="company-name">
                      <span class="input-group-text" id="basic-addon2">.ocaisse.com</span>
                    </div>
                    <label>Email</label>
                    <div class="mb-3">
                      <input type="email" class="form-control" placeholder="Email" aria-label="Email" aria-describedby="email-addon">
                    </div>
                    <label>Password</label>
                    <div class="mb-3">
                      <input type="email" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="password-addon">
                    </div>
                    <div class="form-check form-switch">
                      <input class="form-check-input" type="checkbox" id="rememberMe" checked="">
                      <label class="form-check-label" for="rememberMe">Remember me</label>
                    </div>
                    <div class="text-center">
                      <button type="button" class="btn bg-gradient-info w-100 mt-4 mb-0">Sign in</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-4 text-sm mx-auto">
                    Don't have an account?
                    <a href="javascript:;" class="text-info text-gradient font-weight-bold">Sign up</a>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6 min-vh-100">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('{{ env('APP_URL').'/img/curved-images/curved6.jpg' }}')"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>
@endsection