<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Modules\Hrm\Models\Department;
use App\Modules\Hrm\Models\Shift;
use App\Modules\Hrm\Models\LocationUser;

class User extends Authenticatable
{
    use HasRoles;
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'lang',
        'department_id',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function department() {
        return $this->belongsTo(Department::class,'department_id','id')->select('name');
    }

    public function shifts() {
        return $this->hasMany(Shift::class,'user_id','id');
    }

    public function scopeGetUserShift($query, $location_id, $week_date)
    {
        return Shift::where('location_id',$location_id)
                        ->where('user_id',$this->id)
                        ->where('start_time','LIKE',$week_date.'%')->get();
    }

    public function scopeHasLocation($query, $location)
    {
        return LocationUser::where('user_id',$this->id)
                                ->where('location_id',$location->id)
                                ->count()>0?true:false;
    }

    public function scopeUsersInLocation($query, $location_id)
    {
        return User::whereIn('id',LocationUser::where('location_id',$location_id)->get()->pluck('user_id'))->get();
    }
}
