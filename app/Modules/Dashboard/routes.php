<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

use App\Modules\Oauth\Controllers\Auth\AuthenticatedSessionController;
use App\Modules\Oauth\Controllers\Auth\ConfirmablePasswordController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationNotificationController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationPromptController;
use App\Modules\Oauth\Controllers\Auth\NewPasswordController;
use App\Modules\Oauth\Controllers\Auth\PasswordResetLinkController;
use App\Modules\Oauth\Controllers\Auth\RegisteredUserController;
use App\Modules\Oauth\Controllers\Auth\VerifyEmailController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    'auth',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    Route::get('/', 'HomeController@index')->name('tenancy_dashboard');
});
