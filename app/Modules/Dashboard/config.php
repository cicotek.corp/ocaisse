<?php

/*
|--------------------------------------------------------------------------
| Dashboard Module Configurations
|--------------------------------------------------------------------------
|
| Here you can add configurations that relates to your [Dashboard] module
| Only make sure not to add any other configs that do not relate to this
| Dashboard Module ...
|
*/
return [
    'name' => 'Dashboard'
];