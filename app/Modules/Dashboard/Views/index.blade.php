@extends('dashboard::layouts.main')

@section('title', '')

@section('content')
<section class="pt-3 pb-4" id="count-stats">
    <div class="container">
      <div class="row">
        <div class="col-lg-12  z-index-2 border-radius-xl mt-md-5 mx-auto py-3 blur shadow-blur">
          <div class="row">
            <div class="col-12 text-center mt-3 mb-3">
              <a href="javascript:;" class="avatar avatar-xl rounded-circle m-auto">
                <img alt="Image placeholder" src="https://forrestperkins.com/wp-content/uploads/2017/11/avatar.jpg">
              </a>
              <p class="m-0">Welcome,</p>
              <p style="font-weight: bold;" class="mb-0">{{ Auth::user()->first_name." ".Auth::user()->last_name }}</p>
              <a href="{{ url('oauth/logout') }}">
              <i class="fas fa-sign-out-alt"></i> Log out?
              </a>
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-store"></i></h1>
                <h5 class="mt-3">Web Builder</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-cash-register"></i></h1>
                <h5 class="mt-3">Point of Sale</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-comment-dots"></i></h1>
                <h5 class="mt-3">Messages</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-chart-bar"></i></h1>
                <h5 class="mt-3">ERP</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-boxes"></i></h1>
                <h5 class="mt-3">Inventory</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-tasks"></i></h1>
                <h5 class="mt-3">Tasks</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-video"></i></h1>
                <h5 class="mt-3">Meeting</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-id-card"></i></h1>
                <h5 class="mt-3">CRM</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <a href="{{ route('hrm_departments') }}">
              <div class="p-3 text-center">
                <h1><i class="fas fa-users"></i></h1>
                <h5 class="mt-3">HRM</h5>
              </div>
              </a>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-user"></i></h1>
                <h5 class="mt-3">Profile</h5>
              </div>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <a href="{{ route('company_information') }}">
              <div class="p-3 text-center">
                <h1><i class="fas fa-building"></i></h1>
                <h5 class="mt-3">My Company</h5>
              </div>
              </a>
              <hr class="vertical dark">
            </div>
            <div class="col-md-4 position-relative">
              <div class="p-3 text-center">
                <h1><i class="fas fa-receipt"></i></h1>
                <h5 class="mt-3">My Subcription</h5>
              </div>
              <hr class="vertical dark">
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection