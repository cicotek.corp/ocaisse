<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

use App\Modules\Oauth\Controllers\Auth\AuthenticatedSessionController;
use App\Modules\Oauth\Controllers\Auth\ConfirmablePasswordController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationNotificationController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationPromptController;
use App\Modules\Oauth\Controllers\Auth\NewPasswordController;
use App\Modules\Oauth\Controllers\Auth\PasswordResetLinkController;
use App\Modules\Oauth\Controllers\Auth\RegisteredUserController;
use App\Modules\Oauth\Controllers\Auth\VerifyEmailController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    'auth',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    Route::get('/', 'HomeController@index')->middleware('role_or_permission:Admin|hrm_dashboard.view')->name('hrm_dashboard');

    Route::get('/departments', 'DepartmentController@index')->middleware('role_or_permission:Admin|department.view|department.view_master')->name('hrm_departments');
    Route::get('/departments/create', 'DepartmentController@create')->middleware('role_or_permission:Admin|department.create|department.create_master')->name('hrm_departments_create');
    Route::get('/departments/update/{id}', 'DepartmentController@update')->middleware('role_or_permission:Admin|department.update|department.update_master')->name('hrm_departments_update');
    Route::post('/departments/store', 'DepartmentController@store')->middleware('role_or_permission:Admin|department.create|department.create_master')->name('hrm_departments_store');
    Route::put('/departments/put/{id}', 'DepartmentController@put')->middleware('role_or_permission:Admin|department.update|department.update_master')->name('hrm_departments_put');
    Route::delete('/departments/delete/{id}', 'DepartmentController@delete')->middleware('role_or_permission:Admin|department.delete|department.delete_master')->name('hrm_departments_delete');

    Route::get('/roles', 'RoleController@index')->middleware('role_or_permission:Admin|role.view|role.view_master')->name('hrm_roles');
    Route::post('/roles/store', 'RoleController@store')->middleware('role_or_permission:Admin|role.create|role.create_master')->name('hrm_roles_store');
    Route::put('/roles/put/permissions/{id}', 'RoleController@putPermissions')->middleware('role_or_permission:Admin|role.update|role.update_master')->name('hrm_roles_put_permissions');
    Route::delete('/roles/delete/{id}', 'RoleController@delete')->middleware('role_or_permission:Admin|role.delete|role.delete_master')->name('hrm_roles_delete');

    Route::get('/employees', 'EmployeeController@index')->middleware('role_or_permission:Admin|employee.view|employee.view_master')->name('hrm_employees');
    Route::get('/employees/create', 'EmployeeController@create')->middleware('role_or_permission:Admin|employee.create|employee.create_master')->name('hrm_employees_create');
    Route::get('/employees/update/{id}', 'EmployeeController@update')->middleware('role_or_permission:Admin|employee.update|employee.update_master')->name('hrm_employees_update');
    Route::post('/employees/store', 'EmployeeController@store')->middleware('role_or_permission:Admin|employee.create|employee.create_master')->name('hrm_employees_store');
    Route::put('/employees/put/{id}', 'EmployeeController@put')->middleware('role_or_permission:Admin|employee.update|employee.update_master')->name('hrm_employees_put');
    Route::delete('/employees/delete/{id}', 'EmployeeController@delete')->middleware('role_or_permission:Admin|employee.delete|employee.delete_master')->name('hrm_employees_delete');

    Route::get('/shifts/test', 'ShiftController@test');
    Route::get('/shifts/{week?}/{location_id?}', 'ShiftController@index')->middleware('role_or_permission:Admin|shift.view|employee.shift_master')->name('hrm_shifts');
    Route::post('/shifts/store/{location_id}/{employee_id}/{week_date}', 'ShiftController@store')->middleware('role_or_permission:Admin|shift.create|shift.create_master')->name('hrm_shifts_store');
    Route::delete('/shifts/delete/{id}', 'ShiftController@delete')->middleware('role_or_permission:Admin|shift.delete|shift.delete_master')->name('hrm_shifts_delete');
});
