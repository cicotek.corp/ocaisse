<?php

namespace App\Modules\Hrm\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class LocationUser extends Model
{
    use HasFactory;

    protected $table = 'location_users';
    public $timestamps = false; 
    
    protected $fillable = [
        'user_id',
        'location_id'
    ];
}
