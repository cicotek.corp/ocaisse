<?php

namespace App\Modules\Hrm\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Spatie\Permission\Models\Role;

class Shift extends Model
{
    use HasFactory;

    protected $table = 'shifts';

    protected $fillable = [
        'user_id',
        'location_id',
        'role_id',
        'start_time',
        'end_time',
        'note',
        'publish',
        'shift_status',
        'shift_cancel_employee_msg',
        'shift_cancel_owner_msg',
        'create_by'
    ];

    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function role() {
        return $this->belongsTo(Role::class,'role_id','id');
    }

    public static function getWeekArray($date_formate = 'Y-m-d',$week = 0,$start_day = 'monday')
    {
        // dd($date_formate,$week,$start_day);
        $days_name['monday'] = 0;
        $days_name['tuesday'] = 1;
        $days_name['wednesday'] = 2;
        $days_name['thursday'] = 3;
        $days_name['friday'] = 4;
        $days_name['saturday'] = 5;
        $days_name['sunday'] = 6;        
        $week = $week + $days_name[$start_day];
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('monday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('tuesday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('wednesday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('thursday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('friday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('saturday this week')));
        $week_date[] = date($date_formate, strtotime($week."day",strtotime('sunday this week')));
        return $week_date;
    }
}
