<?php

/*
|--------------------------------------------------------------------------
| Hrm Module Configurations
|--------------------------------------------------------------------------
|
| Here you can add configurations that relates to your [Hrm] module
| Only make sure not to add any other configs that do not relate to this
| Hrm Module ...
|
*/
return [
    'name' => 'Hrm'
];