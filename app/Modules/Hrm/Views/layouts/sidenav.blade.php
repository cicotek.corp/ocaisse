<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0 text-center" href="{{ route('tenancy_dashboard') }}" target="_blank">
        <span class="ms-1 font-weight-bold">{{ tenant('name') }}</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100 h-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        @canany(['hrm_dashboard.view'])
        {{-- <li class="nav-item">
          <a class="nav-link  " href="{{ route('hrm_dashboard') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-chart-line text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li> --}}
        @endcanany
        @canany(['department.view','department.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('hrm_departments') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-building text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Departments</span>
          </a>
        </li>
        @endcanany
        @canany(['employee.view','employee.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('hrm_employees') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-users text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Employees</span>
          </a>
        </li>
        @endcanany
        @canany(['attendance.view','attendance.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="../pages/dashboard.html">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-clipboard-check text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Attendances</span>
          </a>
        </li>
        @endcanany
        @canany(['shift.view','shift.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('hrm_shifts') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-calendar-alt text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Shifts Schedule</span>
          </a>
        </li>
        @endcanany
        @canany(['availability.view','availability.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="../pages/dashboard.html">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-calendar-check text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Availabilities</span>
          </a>
        </li>
        @endcanany
        @canany(['leave.view','leave.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="../pages/dashboard.html">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-calendar-times text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Leaves</span>
          </a>
        </li>
        @endcanany
        @canany(['role.view','role.view_master'])
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('hrm_roles') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fas fa-lock text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Roles & Permissions</span>
          </a>
        </li>
        @endcanany
      </ul>
    </div>
</aside>