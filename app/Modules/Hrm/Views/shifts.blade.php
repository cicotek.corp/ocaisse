@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Shifts')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <form method="GET" action="{{ route('hrm_shifts',[
            'week' => $week
        ]) }}">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex align-items-center">
                        <h6 class="mb-0">Location</h6>
                    </div>
                    <div class="col-md-6 col-12 d-flex align-items-end">
                        <div style="margin-left: auto;">
                            @isset($selected_location)
                                <a href="{{ route('hrm_shifts',[
                                    'location_id' => $selected_location->id,
                                    'week' => $week-7
                                ]) }}"><i class="fa fa-caret-left bg-gradient-dark weak-left"></i></a>
                                <label>{{ $week_date[0] }} to {{ $week_date[6] }}</label>
                                <a href="{{ route('hrm_shifts',[
                                    'location_id' => $selected_location->id,
                                    'week' => $week+7
                                ]) }}"><i class="fa fa-caret-right bg-gradient-dark weak-right"></i></a>
                                <button class="btn btn-sm bg-gradient-dark mx-1 px-3 mb-1 btn-select-shift" type="submit">
                                    Change Location
                                </button>
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-4">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <select name="location_id" class="form-select" aria-label="Location Select" required>
                            @isset($locations)
                                @foreach($locations as $location)
                                    <option value="{{ $location->id }}" {{ (isset($selected_location) && $selected_location->id == $location->id)?'selected':'' }}>{{ $location->name }}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="col-12">
        <div class="card mb-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-6 d-flex align-items-center">
                    <h6 class="mb-0">Shifts Management</h6>
                </div>
            </div>
        </div>
        <div class="card-body mt-3 pt-0 pb-4">
            <div class="table-responsive p-0" id="shift-table">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[0])) }}</span><br><span>{{ $week_date[0] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[1])) }}</span><br><span>{{ $week_date[1] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[2])) }}</span><br><span>{{ $week_date[2] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[3])) }}</span><br><span>{{ $week_date[3] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[4])) }}</span><br><span>{{ $week_date[4] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[5])) }}</span><br><span>{{ $week_date[5] }}</span></th>
                            <th class="text-center text-secondary text-xxs font-weight-bolder opacity-7"><span>{{ date('D', strtotime($week_date[6])) }}</span><br><span>{{ $week_date[6] }}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                                <td class="shifts-place text-center text-secondary text-xxs opacity-7">
                                    <span class="font-weight-bolder">{{ $employee->first_name.' '.$employee->last_name }}</span>
                                </td>
                                @for ($i = 0; $i < 7; $i++)
                                    <td class="shifts-place text-center text-secondary text-xxs font-weight-bolder opacity-7">
                                        <a href="#" class="mr-2 add_shifts"  type="button" data-bs-toggle="modal" data-bs-target="#shift-create-{{ $employee->id }}-{{ $week_date[$i] }}">
                                            <i class="fas fa-plus"></i>
                                        </a>
                                        @foreach ($employee->getUserShift($selected_location->id,$week_date[$i]) as $employee_shift)
                                            <div class="shift-badge">
                                                <b class="mt-1">{{ explode(" ",$employee_shift->start_time)[1].' - '.explode(" ",$employee_shift->end_time)[1] }}</b>
                                                <br>
                                                <span class="mt-5">{{ $employee_shift->role->name }}</span>
                                                <br>
                                                <div class="text-right">
                                                    <form action="{{ route('hrm_shifts_delete', ['id' => $employee_shift->id]) }}" method="post">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button onclick="return confirm('Are you sure to delete this shift?')" type="submit"  class="btn mb-0 bg-transparent p-0 action-item shift-delete">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>                                    
                                        @endforeach
                                    </td>
                                    @include('hrm::components.shift_create_modal',[
                                        'selected_location' => $selected_location,
                                        'employee' => $employee,
                                        'week_date' => $week_date[$i]
                                    ])
                                @endfor
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection