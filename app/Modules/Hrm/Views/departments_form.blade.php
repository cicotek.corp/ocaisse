@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Departments')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <h6 class="mb-0">New Department</h6>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-3">
                @if(isset($department))
                <form method="POST" action="{{ route('hrm_departments_put',['id' => $department->id]) }}">
                @method('PUT')
                @else
                <form method="POST" action="{{ route('hrm_departments_store') }}">
                @endif
                    @csrf
                    <label>Name</label>
                    <div>
                      <input name="name" type="text" class="form-control" placeholder="Department Name" aria-label="name"
                        value="{{ isset($department) ? $department->name : '' }}"
                      >
                    </div>
                    <div class="text-center mt-3">
                      <button type="submit" name="submit" class="btn bg-gradient-info">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection