@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Roles & Permissions')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-md-3 col-12">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <div class="col-6 d-flex align-items-center">
                        <h6 class="mb-0">Roles</h6>
                    </div>
                    <div class="col-6 text-end">
                        <button class="btn btn-sm bg-gradient-dark mb-0 px-3 py-1" type="button" data-bs-toggle="modal" data-bs-target="#roleModal">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-4">
                <div class="list-group" id="list-tab" role="tablist">
                    @foreach($roles as $role)
                        <a class="list-group-item list-group-item-action {{ !$loop->index?'active':'' }}" id="list-permissions-{{ $role->name }}-list" data-bs-toggle="list" href="#list-permissions-{{ $role->id }}" role="tab" aria-controls="list-permissions-{{ $role->id }}">
                            {{ $role->name }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-12">
        <div class="card mb-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <h6 class="mb-0">Permissions</h6>
            </div>
        </div>
        <div class="card-body mt-3 pt-0 pb-4">
            <div class="tab-content" id="nav-tabContent">
                @foreach($roles as $role)
                    @include('hrm::components.permissions_list',['role' => $role])
                @endforeach
            </div>
        </div>
        </div>
    </div>
</div>

@include('hrm::components.role_create_modal')
@endsection