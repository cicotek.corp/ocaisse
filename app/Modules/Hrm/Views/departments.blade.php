@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Departments')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-6 d-flex align-items-center">
                    <h6 class="mb-0">Departments List</h6>
                </div>
                <div class="col-6 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('hrm_departments_create') }}"><i class="fas fa-plus"></i>&nbsp;&nbsp; New Department</a>
                </div>
            </div>
        </div>
        <div class="card-body mt-3 pt-0 pb-4">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                            <!-- <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Employees</th> -->
                            <th class="text-secondary opacity-7"></th>
                            <th class="text-secondary opacity-7"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($departments as $department)
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1">
                                    <h6 class="mb-0 text-sm">{{ $department->name }}</h6>
                                </div>
                            </td>
                            <!-- <td class="align-middle text-center">
                                <p class="text-xs text-secondary mb-0">10</p>
                            </td> -->
                            <td class="align-middle">
                                @if((auth()->user()->department_id == $department->id && auth()->user()->can('department.update')) || auth()->user()->can('department.update_master'))
                                <a href="{{ route('hrm_departments_update',['id' => $department->id]) }}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                    Edit
                                </a>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if((auth()->user()->department_id == $department->id && auth()->user()->can('department.delete')) || auth()->user()->can('department.delete_master'))
                                <form action="{{ route('hrm_departments_delete', ['id' => $department->id]) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button onclick="return confirm('Are you sure to delete this department?')" type="submit" class="btn btn-outline-danger px-2 py-1 mt-4">
                                        Delete
                                    </button>
                                </form>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection