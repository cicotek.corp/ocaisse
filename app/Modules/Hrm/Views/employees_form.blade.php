@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Employees')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <h6 class="mb-0">New Employee</h6>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-3">
                @if(isset($employee))
                <form method="POST" action="{{ route('hrm_employees_put',['id' => $employee->id]) }}">
                @method('PUT')
                @else
                <form method="POST" action="{{ route('hrm_employees_store') }}">
                @endif
                    @csrf
                    <label>Department</label>
                    <div>
                        <select name="department_id" class="form-select" aria-label="Department Select" required>
                            @if(!isset($employee))
                                <option selected>Please select department</option>
                            @endif
                            @isset($departments)
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}" {{ (isset($employee) && $employee->department_id == $department->id)?'selected':'' }}>{{ $department->name }}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>
                    <label>First Name</label>
                    <div>
                      <input name="first_name" type="text" class="form-control" placeholder="First Name" aria-label="name"
                        value="{{ isset($employee) ? $employee->first_name : '' }}"
                        required
                      >
                    </div>
                    <label>Last Name</label>
                    <div>
                        <input name="last_name" type="text" class="form-control" placeholder="Last Name" aria-label="name"
                          value="{{ isset($employee) ? $employee->last_name : '' }}"
                          required
                        >
                    </div>
                    <label>Email</label>
                    <div>
                        <input name="email" type="email" class="form-control" placeholder="Email" aria-label="email"
                          value="{{ isset($employee) ? $employee->email : '' }}"
                          required
                        >
                    </div>
                    <label>Phone</label>
                    <div>
                        <input name="phone" type="text" class="form-control" placeholder="Phone" aria-label="phone"
                          value="{{ isset($employee) ? $employee->phone : '' }}"
                          required
                        >
                    </div>
                    <label>Role</label>
                    <div>
                        @foreach ($roles as $role)
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" name="role_{{ $role->id }}" {{ (isset($employee) && $employee->hasRole($role))?'checked':'' }}>
                                <label class="form-check-label" for="flexSwitchCheckChecked">{{ $role->name }}</label>
                            </div>                          
                        @endforeach
                    </div>
                    <label>Location</label>
                    <div>
                        @foreach ($locations as $location)
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" name="location_{{ $location->id }}" {{ (isset($employee) && $employee->hasLocation($location))?'checked':'' }}>
                                <label class="form-check-label" for="flexSwitchCheckChecked">{{ $location->name }}</label>
                            </div>                          
                        @endforeach
                    </div>
                    <div class="text-center mt-3">
                      <button type="submit" name="submit" class="btn bg-gradient-info">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection