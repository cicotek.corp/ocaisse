@extends('tenant.app.layouts.main')

@section('title', 'HRM')

@section('page_title', 'Employees')

@section('sidenav')
    @include('hrm::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <div class="col-6 d-flex align-items-center">
                        <h6 class="mb-0">Employees List</h6>
                    </div>
                    <div class="col-6 text-end">
                        <a class="btn bg-gradient-dark mb-0" href="{{ route('hrm_employees_create') }}"><i class="fas fa-plus"></i>&nbsp;&nbsp; New Employee</a>
                    </div>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-4">
                <div class="table-responsive p-0">
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="px-0 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                                <th class="px-0 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Department</th>
                                <th class="px-0 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Roles</th>
                                <th class="px-0 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Phone</th>
                                <th class="px-0 text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Email</th>
                                <th class="text-secondary opacity-7"></th>
                                <th class="text-secondary opacity-7"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <td class="px-0">
                                    <div class="d-flex py-1">
                                        <h6 class="mb-0 text-sm">{{ $employee->first_name.' '.$employee->last_name }}</h6>
                                    </div>
                                </td>
                                <td class="px-0">
                                    <div class="d-flex py-1">
                                        <span class="badge bg-gradient-light text-dark">{{ $employee->department->name }}</span>
                                    </div>
                                </td>
                                <td class="px-0">
                                    @foreach ($employee->getRoles() as $role)
                                    <div class="d-flex py-1">
                                        <span class="badge bg-gradient-light text-dark d-block">{{ $role->name }}</span><br>
                                    </div>
                                    @endforeach
                                </td>
                                <td class="px-0">
                                    <div class="d-flex py-1">
                                        <h6 class="mb-0 text-sm">{{ $employee->phone }}</h6>
                                    </div>
                                </td>
                                <td class="px-0">
                                    <div class="d-flex py-1">
                                        <a class="mb-0 text-sm" href="mailto: {{ $employee->email }}"><i class="fas fa-envelope"></i> {{ $employee->email }}</a>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    @if((auth()->user()->department_id == $employee->department->id && auth()->user()->can('employee.update')) || auth()->user()->can('employee.update_master'))
                                    <a href="{{ route('hrm_employees_update',['id' => $employee->id]) }}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit user">
                                        Edit
                                    </a>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    @if((auth()->user()->department_id == $employee->department->id && auth()->user()->can('employee.delete')) || auth()->user()->can('employee.delete_master'))
                                    <form action="{{ route('hrm_employees_delete', ['id' => $employee->id]) }}" method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button onclick="return confirm('Are you sure to delete this employee?')" type="submit" class="btn btn-outline-danger px-2 py-1 mt-4">
                                            Delete
                                        </button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection