<div class="tab-pane fade show  {{ !$loop->index?'active':'' }}" id="list-permissions-{{ $role->id }}" role="tabpanel" aria-labelledby="list-permissions-{{ $role->id }}-list">
    <div class="table-responsive p-0">
        <table class="table table-hover align-items-center mb-0">
            <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Description</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">View</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Create</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Update</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Delete</th>
                </tr>
            </thead>
            <tbody>
                <form method="POST" action="{{ route('hrm_roles_put_permissions',$role->id) }}">
                    @csrf
                    @method('PUT')
                    <tr>
                        <td>
                            <b>Departments Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="department__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="department__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="department__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="department__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="department__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="department__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="department__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="department__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('department.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Employees Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="employee__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="employee__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="employee__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="employee__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="employee__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="employee__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="employee__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="employee__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('employee.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Attendances Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="attendance__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="attendance__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="attendance__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="attendance__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="attendance__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="attendance__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="attendance__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="attendance__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('attendance.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Shifts Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="shift__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="shift__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="shift__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="shift__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="shift__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="shift__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="shift__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="shift__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('shift.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Availabilities Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="availability__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="availability__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="availability__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="availability__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="availability__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="availability__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="availability__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="availability__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('availability.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Leaves Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="leave__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="leave__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="leave__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="leave__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="leave__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="leave__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="leave__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="leave__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('leave.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Roles & Permissions Management</b><br>
                            Basic permissions
                            <br>
                            Advance permissions
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="role__view" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.view'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="role__view_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.view_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="role__create" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.create'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="role__create_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.create_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="role__update" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.update'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="role__update_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.update_master'))?'checked':'' }}>
                        </td>
                        <td class="text-center">
                            <br>
                            <input class="form-check-input" name="role__delete" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.delete'))?'checked':'' }}>
                            <br>
                            <input class="form-check-input" name="role__delete_master" type="checkbox" {{ $role->isAdmin()?'disabled':'' }} {{ ($role->hasPermissionTo('role.delete_master'))?'checked':'' }}>
                        </td>
                    </tr>
                    @if(!$role->isAdmin() && (auth()->user()->can('role.update_master') || (auth()->user()->hasRole($role) && auth()->user()->can('role.update')) ))
                    <tr>
                        <td colspan="4">
                        </td>
                        <td class="text-center">
                            <button class="btn btn-sm bg-gradient-dark mb-0 px-3 py-1" type="submit">Save</button>
                        </td>
                    </tr>
                    @endif
                </form>
                <form action="{{ route('hrm_roles_delete', ['id' => $role->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    @if(!$role->isAdmin() && (auth()->user()->can('role.delete_master') || (auth()->user()->hasRole($role) && auth()->user()->can('role.delete')) ))
                    <tr style="border-style:hidden;">
                        <td class="p-0" colspan="4">
                        </td>
                        <td class="text-center p-0">
                            <button onclick="return confirm('Are you sure to delete this role?')" type="submit" class="btn btn-outline-danger px-3 py-1 m-0">
                                Delete
                            </button>                        
                        </td>
                    </tr>
                    @endif
                </form>
            </tbody>
        </table>
    </div>
</div>
