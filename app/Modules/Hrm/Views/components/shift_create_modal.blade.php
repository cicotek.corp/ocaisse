
                                <div class="modal fade" id="shift-create-{{ $employee->id }}-{{ $week_date }}" tabindex="-1">
                                    <form method="POST" action="{{ route('hrm_shifts_store',[
                                        'location_id' => $selected_location->id,
                                        'employee_id' => $employee->id,
                                        'week_date' => $week_date
                                    ]) }}">
                                        @csrf
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Shift for {{ $employee->first_name.' '.$employee->last_name }} on {{ $week_date }}</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <label>Start Time</label>
                                                <div class="row text-center">
                                                    <div class="col-5">
                                                        <input name="start_hour" type="number" min="0" max="23" class="form-control" required>
                                                    </div>
                                                    <div class="col-2 text-center">:</div>
                                                    <div class="col-5">
                                                        <input name="start_minute" type="number" min="0" max="59" class="form-control" required>
                                                    </div>
                                                </div>
                                                <label>End Time</label>
                                                <div class="row text-center">
                                                    <div class="col-5">
                                                        <input name="end_hour" type="number" min="0" max="23" class="form-control" required>
                                                    </div>
                                                    <div class="col-2 text-center">:</div>
                                                    <div class="col-5">
                                                        <input name="end_minute" type="number" min="0" max="59" class="form-control" required>
                                                    </div>
                                                </div>
                                                <label>Role</label>
                                                <div>
                                                    <select name="role_id" class="form-select" required>
                                                        @foreach ($employee->getRoles() as $role)
                                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn bg-gradient-dark">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>