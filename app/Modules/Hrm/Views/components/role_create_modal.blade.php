<form method="POST" action="{{ route('hrm_roles_store') }}">
    @csrf
    <div class="modal fade" id="roleModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create a new role</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label>Name</label>
                    <div>
                      <input name="name" type="text" class="form-control" placeholder="Role Name" aria-label="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn bg-gradient-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>