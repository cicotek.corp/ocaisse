<?php

namespace App\Modules\Hrm\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hrm\Factory\HrmFactory;
use App\Modules\Hrm\Models\Department;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
        $department = [];
        if(Auth::user()->can('department.view_master')){
            $department = Department::all();
        } else {
            if(Auth::user()->can('department.view')){
                $department = Department::where('id', Auth::user()->department_id)->get();
            }
        }        
        return view('hrm::departments')->with(array(
            'departments' => $department
        ));
    }

    public function create(Request $request)
    {
        return view('hrm::departments_form');
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->can('department.update_master') || (Auth::user()->department_id==$id && Auth::user()->can('department.update'))){
            $department = Department::findOrFail($id);
            return view('hrm::departments_form')->with(array(
                'department' => $department
            ));
        } else {
            abort(403);
        }
    }

    public function store(Request $request)
    {
        if(Auth::user()->can('department.create')){
            Department::create([
                'name' => $request->name
            ]);
            return redirect()->route('hrm_departments')->withSuccess($request->name.' has been created!');
        } else {
            abort(403);
        }
    }

    public function put(Request $request, $id)
    {
        if(Auth::user()->can('department.update_master') || (Auth::user()->department_id==$id && Auth::user()->can('department.update'))){
            Department::where('id',$id)
            ->update([
                'name' => $request->name
            ]);
            return redirect()->route('hrm_departments')->withSuccess($request->name.' has been updated!');
        } else {
            abort(403);
        }
    }

    public function delete(Request $request, $id)
    {
        if(Auth::user()->can('department.delete_master') || (Auth::user()->department_id==$id && Auth::user()->can('department.delete'))){
            $department = Department::findOrFail($id);
            $department->forceDelete();
            return redirect()->route('hrm_departments')->withSuccess($department->name.' has been deleted!');    
        } else {
            abort(403);
        }
    }
}
