<?php

namespace App\Modules\Hrm\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hrm\Factory\HrmFactory;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function index(Request $request)
    {   
        $roles = [];
        if(Auth::user()->can('role.view_master')){
            $roles = Role::all();
        } else {
            if(Auth::user()->can('role.view')){
                $roles = Auth::user()->getRoles();
            }
        }        
        return view('hrm::roles')->with(array(
            'roles' => $roles
        ));
    }

    public function store(Request $request)
    {
        if(Auth::user()->can('role.create')){
            Role::create([
                'name' => $request->name
            ]);
            return redirect()->route('hrm_roles')->withSuccess($request->name.' has been created!');
        } else {
            abort(403);
        }
    }

    public function putPermissions(Request $request, $id)
    {
        if(Auth::user()->can('role.update_master') || (Auth::user()->hasRole($role) && Auth::user()->can('role.update'))){
            $role = Role::findOrFail($id);
            $role->syncPermissions(array_map(function($i){
                return str_replace("__",".",$i);
            }, array_keys(array_filter($request->all(), function($i) {
                return $i == 'on';
            }))));

            return redirect()->route('hrm_roles')->withSuccess($role->name.' has been updated!');
        } else {
            abort(403);
        }
    }

    public function delete(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        if(Auth::user()->can('role.delete_master') || (Auth::user()->hasRole($role) && Auth::user()->can('role.delete'))){
            $role->forceDelete();
            return redirect()->route('hrm_roles')->withSuccess($role->name.' has been deleted!');    
        } else {
            abort(403);
        }
    }
}
