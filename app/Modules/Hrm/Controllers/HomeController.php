<?php

namespace App\Modules\Hrm\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hrm\Factory\HrmFactory;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('hrm::index');
    }

    public function get(Request $request)
    {
        return view('hrm::index');
    }

    public function delete(int $id)
    {
        return view('hrm::index');
    }

    public function edit(int $id=null)
    {
        return view('hrm::index');
    }

    public function submit(Request $request, $id=null)
    {
        return view('hrm::index');
    }
}
