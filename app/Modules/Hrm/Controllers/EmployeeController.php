<?php

namespace App\Modules\Hrm\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hrm\Factory\HrmFactory;
use App\Modules\Hrm\Models\Department;
use App\Modules\Hrm\Models\LocationUser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Modules\Company\Models\Location;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $employees = [];
        if(Auth::user()->can('employee.view_master')){
            $employees = User::all();
        } else {
            if(Auth::user()->can('employee.view')){
                $employees = User::where('department_id', Auth::user()->department_id)->get();
            }
        }        
        return view('hrm::employees')->with(array(
            'employees' => $employees
        ));
    }

    public function create(Request $request)
    {
        $departments = [];
        if(Auth::user()->can('employee.create_master')){
            $departments = Department::all();
        } else {
            if(Auth::user()->can('employee.create')){
                $departments = Department::where('id', Auth::user()->department_id)->get();
            }
        }   
        $roles = [];
        if(Auth::user()->can('employee.create_master')){
            $roles = Role::all();
        } else {
            if(Auth::user()->can('employee.create')){
                $roles = Auth::user()->getRoles();
            }
        }   
        $locations = [];
        $locations = Location::all();

        return view('hrm::employees_form',[
            'departments' => $departments,
            'roles' => $roles,
            'locations' => $locations
        ]);
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->can('employee.update_master') || (Auth::user()->can('employee.update') && $request->department_id == Auth::user()->department_id)){
            $employee = User::findOrFail($id);
            $departments = [];
            if(Auth::user()->can('employee.create_master')){
                $departments = Department::all();
            } else {
                if(Auth::user()->can('employee.create')){
                    $departments = Department::where('id', Auth::user()->department_id)->get();
                }
            }   
            $roles = [];
            if(Auth::user()->can('employee.create_master')){
                $roles = Role::all();
            } else {
                if(Auth::user()->can('employee.create')){
                    $roles = Auth::user()->getRoles();
                }
            }     
            $locations = [];
            $locations = Location::all();
    
            return view('hrm::employees_form')->with(array(
                'employee' => $employee,
                'departments' => $departments,
                'roles' => $roles,
                'locations' => $locations
            ));
        } else {
            abort(403);
        }
    }

    public function store(Request $request)
    {
        if(Auth::user()->can('employee.create_master') || (Auth::user()->can('employee.create') && $request->department_id == Auth::user()->department_id)){

            $employee = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,              
                'department_id' => $request->department_id,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => ""
            ]);

            $roles = array_map(function($i){
                return Role::findOrFail(str_replace("role_","",$i))->name;
            }, array_keys(array_filter($request->all(), function($i) {
                return $i == 'on';
            })));

            $locations = array_map(function($i){
                return str_replace("location_","",$i);
            }, array_keys(array_filter($request->all(), function($i) {
                return $i == 'on';
            })));

            if(count($locations)) {
                foreach($locations as $location) {
                    LocationUser::create([
                        'user_id' => $employee->id,
                        'location_id' => $location->id
                    ]);
                }
            }

            if(count($roles)) $employee->syncRoles($roles);

            return redirect()->route('hrm_employees')->withSuccess($request->last_name.' has been created!');
        } else {
            abort(403);
        }
    }

    public function put(Request $request, $id)
    {
        if(Auth::user()->can('employee.update_master') || (Auth::user()->can('employee.update') && $request->department_id == Auth::user()->department_id)){
            $employee = User::findOrFail($id);
            $employee
                ->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,              
                    'department_id' => $request->department_id,
                    'email' => $request->email,
                    'phone' => $request->phone,
                ]);

            $roles = array_map(function($i){
                return Role::findOrFail(str_replace("role_","",$i))->name;
            }, array_keys(array_filter($request->all(), function($i,$k) {
                return $i == 'on' && str_starts_with($k,'role_');
            },ARRAY_FILTER_USE_BOTH)));

            $locations = array_map(function($i){
                return str_replace("location_","",$i);
            }, array_keys(array_filter($request->all(), function($i,$k) {
                return $i == 'on' && str_starts_with($k,'location_');
            },ARRAY_FILTER_USE_BOTH)));

            if(count($locations)) {
                LocationUser::where('user_id',$employee->id)->forceDelete();
                foreach($locations as $location) {
                    LocationUser::create([
                        'user_id' => $employee->id,
                        'location_id' => $location
                    ]);
                }
            }

            $employee->syncRoles($roles);   

            return redirect()->route('hrm_employees')->withSuccess($request->last_name.' has been updated!');
        } else {
            abort(403);
        }
    }

    public function delete(Request $request, $id)
    {
        if(Auth::user()->can('employee.delete_master') || (Auth::user()->can('employee.delete') && $request->department_id == Auth::user()->department_id)){
            $employee = User::findOrFail($id);
            $employee->forceDelete();
            return redirect()->route('hrm_employees')->withSuccess($employee->last_name.' has been deleted!');    
        } else {
            abort(403);
        }
    }
}
