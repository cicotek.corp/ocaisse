<?php

namespace App\Modules\Hrm\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Hrm\Factory\HrmFactory;
use App\Modules\Hrm\Models\Shift;
use App\Modules\Hrm\Models\Department;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Modules\Company\Models\Location;

class ShiftController extends Controller
{
    public function index(Request $request, $week = 0, $location_id = null)
    {
        $location = null;
        $locations = Location::all();
        if(isset($request->location_id)) $location_id = $request->location_id;
        if(!isset($location_id)){
            if(count($locations)) {
                $location = $locations->first();
            }
        } else {
            $location = Location::findOrFail($location_id);
        }
        $employees = [];
        $employees = User::usersInLocation($location->id);
        return view('hrm::shifts')->with(array(
            'week_date' => Shift::getWeekArray("d-m-Y",$week?$week:0,"monday"),
            'locations' => Location::all(),
            'employees' => $employees,
            'week' => $week,
            'selected_location' => $location,
        ));
    }

    public function create(Request $request)
    {
        return view('hrm::departments_form');
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->can('department.update_master') || (Auth::user()->department_id==$id && Auth::user()->can('department.update'))){
            $department = Department::findOrFail($id);
            return view('hrm::departments_form')->with(array(
                'department' => $department
            ));
        } else {
            abort(403);
        }
    }

    public function store(Request $request, $location_id, $employee_id, $week_date)
    {
        // dd([
        //     'user_id' => $request->employee_id,
        //     'role_id' => $request->role_id,
        //     'location_id' => $location_id,
        //     'start_time' =>  $week_date.' '.$request->start_hour.':'.$request->start_minute,
        //     'end_time' => $week_date.' '.$request->end_hour.':'.$request->end_minute,
        //     'note' => $request->note?$request->note:'',
        //     'create_by' => Auth::user()->id
        // ]);
        Shift::create([
            'user_id' => $request->employee_id,
            'role_id' => $request->role_id,
            'location_id' => $location_id,
            'start_time' =>  $week_date.' '.$request->start_hour.':'.$request->start_minute,
            'end_time' => $week_date.' '.$request->end_hour.':'.$request->end_minute,
            'note' => $request->note?$request->note:'',
            'create_by' => Auth::user()->id
        ]);
        return redirect()->back();
    }

    public function put(Request $request, $id)
    {
        if(Auth::user()->can('department.update_master') || (Auth::user()->department_id==$id && Auth::user()->can('department.update'))){
            Department::where('id',$id)
            ->update([
                'name' => $request->name
            ]);
            return redirect()->route('hrm_departments')->withSuccess($request->name.' has been updated!');
        } else {
            abort(403);
        }
    }

    public function delete(Request $request, $id)
    {
        if(Auth::user()->can('shift.delete_master') || (Auth::user()->department_id==$id && Auth::user()->can('shift.delete'))){
            $shift = Shift::findOrFail($id);
            $shift->forceDelete();
            return redirect()->back();    
        } else {
            abort(403);
        }
    }

    public function test()
    {
        dd(Shift::getUserShift(2,11,'16-08-2021'));
    }
}
