<?php

namespace App\Modules\Oauth\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Oauth\Factory\OauthFactory;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        return view('oauth::index');
    }

    public function get(Request $request)
    {
        return view('oauth::index');
    }

    public function delete(int $id)
    {
        return view('oauth::index');
    }

    public function edit(int $id=null)
    {
        return view('oauth::index');
    }

    public function submit(Request $request, $id=null)
    {
        return view('oauth::index');
    }
}
