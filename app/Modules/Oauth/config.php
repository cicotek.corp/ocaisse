<?php

/*
|--------------------------------------------------------------------------
| Oauth Module Configurations
|--------------------------------------------------------------------------
|
| Here you can add configurations that relates to your [Oauth] module
| Only make sure not to add any other configs that do not relate to this
| Oauth Module ...
|
*/
return [
    'name' => 'Oauth'
];