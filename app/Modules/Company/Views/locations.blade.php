@extends('tenant.app.layouts.main')

@section('title', 'Company')

@section('page_title', 'Locations')

@section('sidenav')
    @include('company::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
        <div class="card-header pb-0 p-3">
            <div class="row">
                <div class="col-6 d-flex align-items-center">
                    <h6 class="mb-0">Locations & Time</h6>
                </div>
                <div class="col-6 text-end">
                    <a class="btn bg-gradient-dark mb-0" href="{{ route('company_locations_create') }}"><i class="fas fa-plus"></i>&nbsp;&nbsp; New Location</a>
                </div>
            </div>
        </div>
        <div class="card-body mt-3 pt-0 pb-4">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Address</th>
                            <th class="text-secondary opacity-7"></th>
                            <th class="text-secondary opacity-7"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($locations as $location)
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1">
                                    <h6 class="mb-0 text-sm">{{ $location->name }}</h6>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex px-2 py-1">
                                    <h6 class="mb-0 text-sm">{{ $location->address }}</h6>
                                </div>
                            </td>
                            <td class="align-middle">
                                <a href="{{ route('company_locations_update',['id' => $location->id]) }}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Edit location">
                                    Edit
                                </a>
                            </td>
                            <td class="align-middle">
                                <form action="{{ route('company_locations_delete', ['id' => $location->id]) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button onclick="return confirm('Are you sure to delete this location?')" type="submit" class="btn btn-outline-danger px-2 py-1 mt-4">
                                        Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection