@extends('tenant.app.layouts.main')

@section('title', 'Company')

@section('page_title', 'Information')

@section('sidenav')
    @include('company::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
  <div class="col-12">
        <form method="POST" action="{{ route('company_information_put') }}">
        @method('PUT')
        @csrf
          <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <h6 class="mb-0">Company Information</h6>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-3">
              <div class="row">    
                <div class="col-md-6 col-12">
                  <label>Name</label>
                  <div>
                    <input name="name" type="text" class="form-control" placeholder="Company Name" aria-label="name"
                      value="{{ isset(tenant()->name) ? tenant()->name : '' }}"
                    >
                  </div>
                  <label>Office Address</label>
                  <div>
                    <input name="office_address" type="text" class="form-control" placeholder="Office Address" aria-label="address"
                      value="{{ isset(tenant()->office_address) ? tenant()->office_address : '' }}"
                    >
                  </div>
                  <label>Code Postal</label>
                  <div>
                    <input name="code_postal" type="number" class="form-control" placeholder="Code Postal" aria-label="codepostal"
                      value="{{ isset(tenant()->code_postal) ? tenant()->code_postal : '' }}"
                    >
                  </div>
                  <label>Country</label>
                  <div>
                    <input name="country" type="text" class="form-control" placeholder="Country" aria-label="country"
                      value="{{ isset(tenant()->country) ? tenant()->country : '' }}"
                    >
                  </div>
                  <label>Description</label>
                  <div>
                    <textarea name="description" type="text" class="form-control" placeholder="Description" aria-label="description"
                      rows="4"
                    >{{ isset(tenant()->description) ? tenant()->description : '' }}
                    </textarea>
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <label>Representative Name</label>
                  <div>
                    <input name="representative" type="text" class="form-control" placeholder="Representative Name" aria-label="name"
                      value="{{ isset(tenant()->representative) ? tenant()->representative : '' }}"
                    >
                  </div>
                  <label>Contact Number</label>
                  <div>
                    <input name="phone" type="text" class="form-control" placeholder="Phone" aria-label="name"
                        value="{{ isset(tenant()->phone) ? tenant()->phone : '' }}">
                  </div>
                  <label>City</label>
                  <div>
                    <input name="city" type="text" class="form-control" placeholder="City" aria-label="city"
                      value="{{ isset(tenant()->city) ? tenant()->city : '' }}"
                    >
                  </div>
                </div>
              </div>
              <div class="text-center mt-3">
                <button type="submit" name="submit" class="btn bg-gradient-info">Submit</button>
              </div>
            </div>
          </div>
        </form>
  </div>
</div>
@endsection