@extends('tenant.app.layouts.main')

@section('title', 'Company')

@section('page_title', 'Locations')

@section('sidenav')
    @include('company::layouts.sidenav')
@endsection

@section('content')
<div class="row min-vh-75">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-header pb-0 p-3">
                <div class="row">
                    <h6 class="mb-0">New Location</h6>
                </div>
            </div>
            <div class="card-body mt-3 pt-0 pb-3">
                @if(isset($location))
                <form method="POST" action="{{ route('company_locations_put',['id' => $location->id]) }}">
                @method('PUT')
                @else
                <form method="POST" action="{{ route('company_locations_store') }}">
                @endif
                    @csrf
                    <label>Name</label>
                    <div>
                      <input name="name" type="text" class="form-control" placeholder="Name" aria-label="name"
                        value="{{ isset($location) ? $location->name : '' }}"
                      >
                    </div>
                    <label>Address</label>
                    <div>
                      <input name="address" type="text" class="form-control" placeholder="Address" aria-label="address"
                        value="{{ isset($location) ? $location->address : '' }}"
                      >
                    </div>
                    <div class="text-center mt-3">
                      <button type="submit" name="submit" class="btn bg-gradient-info">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection