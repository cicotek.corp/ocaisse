<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0 text-center" href="{{ route('tenancy_dashboard') }}" target="_blank">
        <span class="ms-1 font-weight-bold">{{ tenant('name') }}</span>
      </a>
    </div>
    <hr class="horizontal dark mt-0">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100 h-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        {{-- @canany(['hrm_dashboard.view']) --}}
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('company_information') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fas fa-info-circle text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Information</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('company_locations') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fas fa-map-marked text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Locations & Time</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('company_information') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fas fa-cogs text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Configurations</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link  " href="{{ route('company_information') }}">
            <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fas fa-envelope text-dark"></i>
            </div>
            <span class="nav-link-text ms-1">Mail Templates</span>
          </a>
        </li>
        {{-- @endcanany --}}
      </ul>
    </div>
</aside>