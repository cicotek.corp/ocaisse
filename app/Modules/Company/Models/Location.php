<?php

namespace App\Modules\Company\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Location extends Model
{
    use HasFactory;

    protected $table = 'tenant_locations';

    protected $fillable = [
        'name',
        'address',
        'tenant_id'
    ];
}
