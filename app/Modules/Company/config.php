<?php

/*
|--------------------------------------------------------------------------
| Company Module Configurations
|--------------------------------------------------------------------------
|
| Here you can add configurations that relates to your [Company] module
| Only make sure not to add any other configs that do not relate to this
| Company Module ...
|
*/
return [
    'name' => 'Company'
];