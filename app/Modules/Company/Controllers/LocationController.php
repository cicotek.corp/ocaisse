<?php

namespace App\Modules\Company\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Company\Factory\CompanyFactory;
use App\Models\Tenant;
use App\Modules\Company\Models\Location;

class LocationController extends Controller
{
    public function index(Request $request)
    {
        $locations = Location::all();
        return view('company::locations',[
            'locations' => $locations
        ]);
    }

    public function update(Request $request, $id)
    {
        $location = Location::findOrFail($id);
        return view('company::locations_form')->with(array(
            'location' => $location
        ));
    }

    public function store(Request $request)
    {
        Location::create([
            'tenant_id' => tenant('id'),
            'name' => $request->name,
            'address' => $request->address
        ]);
        return redirect()->route('company_locations')->withSuccess($request->name.' has been created!');
        // tenant()->update(array_filter($request->input()));
    }

    public function put(Request $request, $id)
    {
        Location::where('id',$id)
        ->update([
            'tenant_id' => tenant('id'),
            'name' => $request->name,
            'address' => $request->address
        ]);
        return redirect()->route('company_locations')->withSuccess($request->name.' has been updated!');
    }

    public function create(Request $request)
    {
        return view('company::locations_form');
    }
}
