<?php

namespace App\Modules\Company\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Company\Factory\CompanyFactory;
use App\Models\Tenant;

class InformationController extends Controller
{
    public function index(Request $request)
    {
        return view('company::index');
    }

    public function put(Request $request)
    {
        tenant()->update(array_filter($request->input()));
        return redirect()->route('company_information')->withSuccess($request->name.' has been updated!');
    }
}
