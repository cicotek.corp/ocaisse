<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;

use App\Modules\Oauth\Controllers\Auth\AuthenticatedSessionController;
use App\Modules\Oauth\Controllers\Auth\ConfirmablePasswordController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationNotificationController;
use App\Modules\Oauth\Controllers\Auth\EmailVerificationPromptController;
use App\Modules\Oauth\Controllers\Auth\NewPasswordController;
use App\Modules\Oauth\Controllers\Auth\PasswordResetLinkController;
use App\Modules\Oauth\Controllers\Auth\RegisteredUserController;
use App\Modules\Oauth\Controllers\Auth\VerifyEmailController;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here you can register the tenant routes for your application.
| These routes are loaded by the TenantRouteServiceProvider.
|
| Feel free to customize them however you want. Good luck!
|
*/

Route::middleware([
    'web',
    'auth',
    InitializeTenancyByDomain::class,
    PreventAccessFromCentralDomains::class,
])->group(function () {
    Route::get('/', 'InformationController@index')->name('company_information');
    Route::put('/info/update', 'InformationController@put')->name('company_information_put');

    Route::get('/locations', 'LocationController@index')->name('company_locations');
    Route::get('/locations/create', 'LocationController@create')->name('company_locations_create');
    Route::get('/locations/update/{id}', 'LocationController@update')->name('company_locations_update');
    Route::post('/locations/store', 'LocationController@store')->name('company_locations_store');
    Route::put('/locations/put/{id}', 'LocationController@put')->name('company_locations_put');
    Route::delete('/locations/delete/{id}', 'LocationController@delete')->name('company_locations_delete');
});
