<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('location_id');
            $table->string('start_time',50)->nullable();
            $table->string('end_time',50)->nullable();
            $table->text('note')->nullable();
            $table->integer('publish')->default(1)->comment('0=>unpublish/1=>publish');
            $table->string('shift_status')->default('enable')->comment('enable/disable/request');
            $table->string('shift_cancel_employee_msg')->nullable();
            $table->string('shift_cancel_owner_msg')->nullable();
            $table->integer('create_by')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('tenant_locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
